# Walkable Beautiful Bridge Railways

Makes [Beautiful Bridge Railways by kapaer](https://mods.factorio.com/mod/beautiful_bridge_railway) walkable.

- When a bridge is placed, shallow water is created beneath the bridge, making the terrain walkable.
- When a bridge is mined, shallow water beneath the bridge is replaced by deep green water to simulate the environmental impact of the bridge -- _but also because I haven't found a good way to do this correctly; open to suggestions via PR_.
