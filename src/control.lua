local function table_insert_list(tbl, items)
	for _, i in ipairs(items) do
		table.insert(tbl, i)
	end
	return tbl
end

local function replace_tiles_in_area(surface, area, name_old_matcher, name)
	local tiles = {}
	for x = area.left_top.x, area.right_bottom.x do
		for y = area.left_top.y, area.right_bottom.y do
			local t_old = surface.get_tile(x, y);
			if string.match(t_old.name, name_old_matcher) then
				local tile = {}
				tile.position = { x, y }
				tile.name = name
				table.insert(tiles, tile)
			end
		end
	end
	return tiles
end

local function make_walkable(event)
	if string.match(event.created_entity.name, "^bbr-") then
		local tiles = {}
		local bounding_boxes = { event.created_entity.bounding_box, event.created_entity.secondary_bounding_box }
		for _, bounds in ipairs(bounding_boxes) do
			table_insert_list(tiles,
				replace_tiles_in_area(event.created_entity.surface, bounds, "^water", "water-shallow"))
			table_insert_list(tiles,
				replace_tiles_in_area(event.created_entity.surface, bounds, "^deepwater", "water-shallow"))
		end
		event.created_entity.surface.set_tiles(tiles, true, false)
	end
end

local function remove_bridge(event)
	if not string.match(event.entity.name, "^bbr-") then return end
	local surface = event.entity.surface;
	local tiles = {}

	-- Includes the entity which was removed
	local overlapping_entities = surface.count_entities_filtered {
		area = event.entity.bounding_box,
		name = {
			"bbr-rail-wood",
			"bbr-straight-rail-wood",
			"bbr-curved-rail-wood",
			"bbr-rail-iron",
			"bbr-straight-rail-iron",
			"bbr-curved-rail-iron",
			"bbr-rail-brick",
			"bbr-straight-rail-brick",
			"bbr-curved-rail-brick"
		}
	}

	if overlapping_entities == 1 then
		-- Remove Bridge
		table_insert_list(tiles,
			replace_tiles_in_area(surface, event.entity.bounding_box, "^water%-shallow", "deepwater-green"))
	end

	if (event.entity.secondary_bounding_box ~= nil) then
		-- Includes the entity which was removed
		local secondary_overlapping_entities = surface.count_entities_filtered {
			area = event.entity.secondary_bounding_box,
			name = {
				"bbr-rail-wood",
				"bbr-straight-rail-wood",
				"bbr-curved-rail-wood",
				"bbr-rail-iron",
				"bbr-straight-rail-iron",
				"bbr-curved-rail-iron",
				"bbr-rail-brick",
				"bbr-straight-rail-brick",
				"bbr-curved-rail-brick"
			}
		}
		if secondary_overlapping_entities == 1 then
			-- Remove Bridge
			table_insert_list(tiles,
				replace_tiles_in_area(surface, event.entity.secondary_bounding_box, "^water%-shallow",
					"deepwater-green"))
		end
	end
	surface.set_tiles(tiles, true, false)
end

script.on_event(defines.events.on_built_entity, make_walkable)
script.on_event(defines.events.on_robot_built_entity, make_walkable)

script.on_event(defines.events.on_player_mined_entity, remove_bridge)
script.on_event(defines.events.on_robot_mined_entity, remove_bridge)
script.on_event(defines.events.on_entity_died, remove_bridge)
